from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class UserProperty(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    no_of_search = models.IntegerField(blank=True, null=True, default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return u"%s" % self.user

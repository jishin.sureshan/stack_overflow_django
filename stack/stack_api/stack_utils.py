import requests
import json
import webbrowser
import urllib.parse
from django.conf import settings

access_token = "uHK8dFNoHyE9Wzqg3xfy8A))"


class StackApi(object):
    def __init__(self):
        self.headers = {
            'Authorization': 'token {}'.format(access_token),
            'content-type': 'application/x-www-form-urlencoded'
        }
        # print(page)
        self.client_id = "22630",
        self.client_secret = 'FO6IoI8W2i4NT0A5SED4Bw((',
        self.redirect_uri = "https://stackexchange.com/oauth/login_success"
        self.scope = "read_inbox,no_expiry",
        self.token_url = "https://stackoverflow.com/oauth/access_token"
        self.auth_url = 'https://stackoverflow.com/oauth/dialog',
        self.base_url = "https://api.stackexchange.com/2.3/",

    def api_outh(self):
        auth_url = "%s?client_id=%s&redirect_uri=%s&scope=%s" % (
            self.auth_url, self.client_id, self.redirect_uri, self.scope)
        url = requests.request('POST', auth_url)
        return url.url

    def get_token(self):
        redirect_url = urllib.parse.quote(self.redirect_uri)
        payload = {
            'client_id': '22630',
            'client_secret': 'FO6IoI8W2i4NT0A5SED4Bw((',
            'code': "qsH7avh4k(JYZkb(tKABYA))",
            'redirect_uri': redirect_url,
            'grant_type': 'authorization_code'
        }

        headers = {'content-type': 'application/x-www-form-urlencoded'}
        url = requests.request('POST', self.token_url, data=payload, headers=headers)
        return url.text

    def search_stack(self, start_page, total_count, closed_val, search):
        request_url = "https://api.stackexchange.com/2.3/search/advanced?page={}&pagesize={}&closed={}&order=desc&sort=activity&q={}&site=stackoverflow".format(
            int(start_page), int(total_count), closed_val, search)
        search_result = requests.request('GET', request_url, headers=self.headers)
        return search_result.json()

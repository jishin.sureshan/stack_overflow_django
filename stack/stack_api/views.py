from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UserSerializer
from .stack_utils import StackApi
from datetime import datetime, timedelta
from django.utils import timezone
from .models import UserProperty
from django.core.cache import cache
from rest_framework import status
from rest_framework.throttling import UserRateThrottle
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
import json

MAX_SEARCH = 50
MAX_SEARCH_PER_MINUTE = 5


# Create your views here.

class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserMinThrottle(UserRateThrottle):
    scope = 'user_min'


class UserDayThrottle(UserRateThrottle):
    scope = 'user_day'


class StackApiView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    throttle_classes = [
        UserMinThrottle,
        UserDayThrottle
    ]

    def get(self, *args, **kwargs):
        query = self.request.GET.get('search', None)
        closed = self.request.GET.get('closed', None)
        page = self.request.GET.get('page', 1)
        count = self.request.GET.get('pagecount', None)
        closed = closed if closed else None
        closed_val = True if closed == 'True' else False
        page_val = page if page else 1
        count_val = count if count else 100
        stack = StackApi()
        data = stack.search_stack(page_val, count_val, closed_val, query)
        cache.get_or_set('search', data, 30)
        initial_search = 0
        if 'items' in data and data['items']:
            result = data['items']
            user_property, created = UserProperty.objects.get_or_create(user=self.request.user)
            user_property.no_of_search = initial_search + user_property.no_of_search + 1
            user_property.save()
            return Response(result, status=status.HTTP_200_OK)
        else:
            return Response([], status=status.HTTP_400_BAD_REQUEST)

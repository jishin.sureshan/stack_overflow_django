from django.contrib import admin
from .models import UserProperty


# from reversion.admin import VersionAdmin
# Register your models here.

class UserPropertyAdmin(admin.ModelAdmin):
    list_display = ("user", "no_of_search")


admin.site.register(UserProperty, UserPropertyAdmin)
